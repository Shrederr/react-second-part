export function getMessages(messageList) {
    return {
        type: "GET_MESSAGES",
        messageList
    }
}

export function sendMessage(newMessage) {
    return {
        type: "SEND_MESSAGE",
        payload: newMessage
    }
}

export function setMessage(message) {
    return {
        type: "SET_MESSAGE",
        payload: message
    }
}

export function likeMessage(id) {
    return {
        type: "LIKE_MESSAGE",
        payload: id
    }
}

export function deleteMessage(id) {
    return {
        type: "DELETE_MESSAGE",
        payload: id
    }
}

export function switchEditor(status) {
    return {
        type: "SWITCH_EDITOR",
        payload: status
    }
}

export function sendEditMessage(newMessage) {
    return {
        type: "SEND_EDIT_MESSAGE",
        payload: newMessage
    }
}

