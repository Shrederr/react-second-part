const initialState = {
    userCounts: 20,
    messageCounts: 0,
    lastMessageDate: "2020-07-16T19:48:42.481Z",
    messageList: [],
    message: "",
    editMessage: "",
    newEditMessage: "",
    likes: [],
    mutableMessage: {
        isOpen: false,
        id: ""
    },
    spinner: true
}


export function chatReducer(state = initialState, action) {
    let index = 0;
    switch (action.type) {
        case "GET_MESSAGES":
            if (!("messageList" in action)) {
                return state;
            }
            return Object.assign({}, state, {
                messageList: action.messageList,
                currentUser: action.messageList[0],
                messageCounts: action.messageList.length,
                userCounts: 20,
                spinner: false
            })
        case "SEND_MESSAGE":
            action.payload.text = state.message;
            return {
                ...state,
                messageList: [...state.messageList, action.payload],
                lastMessageDate: action.payload.createdAt,
                messageCounts: state.messageList.length + 1,
                message: ""
            }
        case "SET_MESSAGE":
            return {
                ...state,
                message: action.payload
            }
        case "LIKE_MESSAGE":
            index = state.likes.indexOf(action.payload);
            if (index === -1) {
                return {
                    ...state,
                    likes: [...state.likes, action.payload]
                }
            } else {
                const newLikes = [...state.likes]
                newLikes.splice(index, 1);
                return {
                    ...state,
                    likes: newLikes
                }
            }
        case "DELETE_MESSAGE":
            index = state.messageList.findIndex((item) => {
                return item.id === action.payload
            });
            const newList = [...state.messageList];
            const newDate = state.messageList[index - 1].createdAt;
            const newCountsMessage = state.messageCounts - 1;
            newList.splice(index, 1);
            return {
                ...state,
                messageList: newList,
                lastMessageDate: newDate,
                messageCounts: newCountsMessage
            }
        case "SWITCH_EDITOR":
            return {
                ...state,
                mutableMessage: {
                    isOpen: action.payload.isOpen,
                    id: action.payload.id
                }
            }

        case "SEND_EDIT_MESSAGE": {
            index = state.messageList.findIndex((item) => {
                return item.id === state.mutableMessage.id
            });
            const editDate = new Date().toISOString();
            const newList = [...state.messageList];
            newList[index].text = action.payload;
            newList[index].editedAt = editDate;
            return {
                ...state,
                messageList: newList
            }
        }


        default:
            return state;
    }

}
