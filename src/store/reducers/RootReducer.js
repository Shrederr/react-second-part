import {chatReducer} from "./chatReducer";
import {combineReducers} from "redux";


const RootReducer = combineReducers({
    chat: chatReducer,
});

export default RootReducer;
