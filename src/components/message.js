import React from "react";
import "../styles/message.css";


export default class Message extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        let classList = "message-wrapper";
        let deleteMessage = "deleteMessage"
        let likeClass = "like";
        let editMessage = "editMessage"
        let avatar = (
            <div className="avatar">
                <img src={this.props.avatar}/>
            </div>
        )
        if (this.props.isClient) {
            classList += " client-message";
            deleteMessage += " isClient";
            editMessage += " isClient";
            avatar = "";
        }

        if (this.props.isLike && !this.props.isClient) {
            likeClass += " liked";
        }

        return (
            <div className={classList}>
                {avatar}
                <div className="userName">
                    {this.props.name}
                </div>
                <div className="text-wrapper">
                    {this.props.text}
                </div>
                <div className="createdData">
                    {this.props.createdAt}
                </div>
                <div className={likeClass} onClick={this.props.likeMessage}>
                    <svg viewBox="0 0 24 24">
                        <path className="a"
                              d="M20,15.659h0a1.5,1.5,0,1,1,0,3H19a1.5,1.5,0,0,1,1.5,1.5c0,.829-.672,1-1.5,1H12.5c-2.851,0-3.5-.5-7-1v-8.5c2.45,0,6.5-4.5,6.5-8.5,0-1.581,2.189-2.17,3,.719.5,1.781-1,5.281-1,5.281h8a1.5,1.5,0,0,1,1.5,1.5c0,.829-.672,2-1.5,2H21a1.5,1.5,0,0,1,0,3H20"/>
                        <path d="M3.25,19.159a.75.75,0,1,0,.75.75.75.75,0,0,0-.75-.75Z"/>
                    </svg>
                </div>
                <div className={deleteMessage} onClick={this.props.deleteMessage}>
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <title>spam, trash, bin, garbage, delete</title>
                        <path d="M2.88,5,5.11,24H18.89L21.12,5ZM17.11,22H6.89L5.12,7H18.88Z"/>
                        <polygon points="21 2 15 2 15 1 13 1 13 0 11 0 11 1 9 1 9 2 3 2 3 4 21 4 21 2"/>
                    </svg>
                </div>
                <div className={editMessage} onClick={this.props.openEditor}>
                    <svg height="401pt" viewBox="0 -1 401.52289 401" width="401pt">
                        <path
                            d="m370.589844 250.972656c-5.523438 0-10 4.476563-10 10v88.789063c-.019532 16.5625-13.4375 29.984375-30 30h-280.589844c-16.5625-.015625-29.980469-13.4375-30-30v-260.589844c.019531-16.558594 13.4375-29.980469 30-30h88.789062c5.523438 0 10-4.476563 10-10 0-5.519531-4.476562-10-10-10h-88.789062c-27.601562.03125-49.96875 22.398437-50 50v260.59375c.03125 27.601563 22.398438 49.96875 50 50h280.589844c27.601562-.03125 49.96875-22.398437 50-50v-88.792969c0-5.523437-4.476563-10-10-10zm0 0"/>
                        <path
                            d="m376.628906 13.441406c-17.574218-17.574218-46.066406-17.574218-63.640625 0l-178.40625 178.40625c-1.222656 1.222656-2.105469 2.738282-2.566406 4.402344l-23.460937 84.699219c-.964844 3.472656.015624 7.191406 2.5625 9.742187 2.550781 2.546875 6.269531 3.527344 9.742187 2.566406l84.699219-23.464843c1.664062-.460938 3.179687-1.34375 4.402344-2.566407l178.402343-178.410156c17.546875-17.585937 17.546875-46.054687 0-63.640625zm-220.257812 184.90625 146.011718-146.015625 47.089844 47.089844-146.015625 146.015625zm-9.40625 18.875 37.621094 37.625-52.039063 14.417969zm227.257812-142.546875-10.605468 10.605469-47.09375-47.09375 10.609374-10.605469c9.761719-9.761719 25.589844-9.761719 35.351563 0l11.738281 11.734375c9.746094 9.773438 9.746094 25.589844 0 35.359375zm0 0"/>
                    </svg>
                </div>

            </div>
        )

    }
}
