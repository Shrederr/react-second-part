import React, {useEffect, useState} from 'react';
import Header from "./header";
import MessageList from "./message-list";
import MessageInput from "./input";
import {useDispatch, useSelector} from "react-redux"
import * as ChatActions from "../store/ChatActions"
import uuid from 'react-uuid';
import EditorMessage from "../components/editModal";
import LoadingSpinner from "../components/spiner";

const Chat = ({chatName, currentUserId, currentUserName, currentUserAvatar, isClient}) => {
    const API_LINK = 'https://edikdolynskyi.github.io/react_sources/messages.json';
    const chat = useSelector(state => state.chat);
    const dispatch = useDispatch();
    const [newEditMessage, setNewEditMessage] = useState("")
    const fetchData = async () => {
        return fetch(API_LINK).then((response) => response.json());
    };

    function sortByDate(arr) {
        arr.sort((a, b) => new Date(a.createdAt).getTime() > new Date(b.createdAt).getTime() ? 1 : -1);
    }

    function likeMessage(id) {
        dispatch(ChatActions.likeMessage(id))
    }

    function deleteMessage(id) {
        dispatch(ChatActions.deleteMessage(id))
    }

    function openEditor(id) {
        if (id === chat.messageList[chat.messageList.length - 1].id) {
            dispatch(ChatActions.switchEditor({
                isOpen: true,
                id: id
            }))
            setNewEditMessage('');
        }
    }

    function closeEditor() {
        dispatch(ChatActions.switchEditor({
            isOpen: false,
            id: ""
        }))
    }

    function sendEditMessage() {
        dispatch(ChatActions.sendEditMessage(newEditMessage));
        closeEditor();
    }

    function sendMessage() {
        const date = new Date().toISOString();
        const newMessage = {
            id: uuid(),
            userId: currentUserId,
            avatar: currentUserAvatar,
            user: currentUserName,
            createdAt: date,
            editedAt: date,
            isClient: isClient
        }
        if (!chat.message.replace(/\s+/g, '')) {
            return;
        }
        dispatch(ChatActions.sendMessage(newMessage));
    }

    function changeMessage(e) {
        dispatch(ChatActions.setMessage(e.target.value));
    }

    function changeNewMessage(e) {
        setNewEditMessage(e.target.value);
    }

    useEffect(() => {
        const _fetchData = async () => {
            const messageList = await fetchData();
            sortByDate(messageList);
            dispatch(ChatActions.getMessages(messageList));

        };
        _fetchData();
    }, []);


    if (!chat.spinner) {
        return (

            <div>
                <Header
                    chatName={chatName}
                    usersCounts={chat.userCounts}
                    messagesCounts={chat.messageCounts}
                    lastMessageDate={chat.lastMessageDate}
                />
                <MessageList messageList={chat.messageList}
                             currentUserId={currentUserId}
                             likeMessage={likeMessage}
                             likeList={chat.likes}
                             deleteMessage={deleteMessage}
                             editMessage={openEditor}
                />
                <EditorMessage isOpen={chat.mutableMessage.isOpen} isClose={closeEditor} messageText={newEditMessage}
                               changeNewMessage={changeNewMessage}
                               sendEditMessage={sendEditMessage}/>
                <MessageInput sendMessage={sendMessage} changeMessage={changeMessage} message={chat.message}/>
            </div>
        )
    } else {
        return (
            <div>
                <LoadingSpinner/>
            </div>
        )
    }
}


export default (Chat);