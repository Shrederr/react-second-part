import React from "react";
import "../styles/header.css";


export default class Header extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div className="chat_header">
                <div className="chat_info">
                    <div className="chatName headerBlock">{this.props.chatName}</div>
                    <div className="users_counts headerBlock">{this.props.usersCounts + " " + "participants"}</div>
                    <div className="messages_counts headerBlock">{this.props.messagesCounts + " " + "messages"}</div>
                    <div
                        className="last_message_date headerBlock">{"last message at" + " " + new Date(this.props.lastMessageDate).toDateString()}</div>
                </div>
            </div>
        )

    }
}

